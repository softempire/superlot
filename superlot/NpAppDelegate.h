//
//  NpAppDelegate.h
//  superlot
//
//  Created by Summer Ding on 5/5/12.
//  Copyright (c) 2012 www.napeso.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NpAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
