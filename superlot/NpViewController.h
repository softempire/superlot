//
//  NpViewController.h
//  superlot
//
//  Created by Summer Ding on 5/5/12.
//  Copyright (c) 2012 www.napeso.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface NpViewController : UIViewController
{
    BOOL isRunning;
    NSArray *names;
    NSArray *nameAudioes;
}

- (IBAction)timerStep:(NSTimer *)theTimer;
- (IBAction)tapped:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *currentNameLabel;
@property (nonatomic) AVAudioPlayer *audioPlayer;

@property NSTimer *timer;

@end
