//
//  NpViewController.m
//  superlot
//
//  Created by Summer Ding on 5/5/12.
//  Copyright (c) 2012 www.napeso.com. All rights reserved.
//

#import "NpViewController.h"
#import "ISSpeechSynthesis.h"
#import <AudioToolbox/AudioToolbox.h>

@interface NpViewController ()

@end

@implementation NpViewController
@synthesize currentNameLabel;
@synthesize audioPlayer;

@synthesize timer;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    srand(time(NULL));
    isRunning = NO;
    names = [NSArray arrayWithObjects:@"Summer Ding", 
                                      @"Tristone Hua", 
                                      @"Johnny Huang",
                                      @"Paul Chen",
                                      @"Tony Huang",
                                      @"David Zhong",
                                      @"Zeyi Xia",
                                      @"Apple Gao",
                                      @"Terry Zhou",
                                      nil];
    
    nameAudioes = [NSArray arrayWithObjects:@"SD", 
                                            @"TH", 
                                            @"JH",
                                            @"PC",
                                            @"TonyH",
                                            @"DZ",
                                            @"RX",
                                            @"AG",
                                            @"TZ",
                                            nil];
}

- (void)viewDidUnload
{
    [self setCurrentNameLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)timerStep:(NSTimer *)theTimer
{
    for(int i = 0; i < 100; i++)
    {
        rand();
    }
    
    int randValue = rand();
    NSLog(@"%d", randValue);
    int index = randValue % names.count;
    self.currentNameLabel.text = [names objectAtIndex:index];
}

- (IBAction)tapped:(id)sender 
{
    if (!isRunning)
    {
        timer = [NSTimer timerWithTimeInterval:0.05 target:self selector:@selector(timerStep:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    }
    else 
    {
        [timer invalidate];
    
        NSString *name = self.currentNameLabel.text;
        int index = [names indexOfObject:name];
        NSString *nameAudio = [nameAudioes objectAtIndex:index];
    
        SystemSoundID soundID; 
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.mp3", [[NSBundle mainBundle] resourcePath], nameAudio]];
        
        AudioServicesCreateSystemSoundID((__bridge_retained CFURLRef)url, &soundID); 
        AudioServicesPlaySystemSound (soundID);
    }
    
    isRunning = !isRunning;
}

@end
