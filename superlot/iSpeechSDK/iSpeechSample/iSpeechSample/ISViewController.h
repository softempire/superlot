//
//  ISViewController.h
//  iSpeechSample
//
//  Created by Grant Butler on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ISpeechSDK.h"

@interface ISViewController : UIViewController <ISSpeechRecognitionDelegate>

@property (nonatomic,retain) IBOutlet UILabel *label;

@end
