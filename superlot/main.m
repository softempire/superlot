//
//  main.m
//  superlot
//
//  Created by Summer Ding on 5/5/12.
//  Copyright (c) 2012 www.napeso.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NpAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NpAppDelegate class]));
    }
}
